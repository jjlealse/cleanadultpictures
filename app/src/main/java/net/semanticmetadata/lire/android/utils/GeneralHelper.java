package net.semanticmetadata.lire.android.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.ZipOutputStream;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

public class GeneralHelper {


	public static String[] getVideoFileList() {
		File rootsd = Environment.getExternalStorageDirectory();
		File dcim = new File(rootsd.getAbsolutePath() + "/DCIM/Camera");

		// String[] videoFileList = dcim.list();
		String[] videoFileList = dcim.list(new FilenameFilter() {

			public boolean accept(File arg0, String arg1) {
				if (arg1.endsWith(".3gp") || arg1.endsWith(".mp4"))
					return true;
				return false;
			}
		});
		return videoFileList;
	}

	public static String getStandardPictureVideoPath(){
		 File rootsd = Environment.getExternalStorageDirectory();
	       return rootsd.getAbsolutePath() + "/DCIM/Camera";
	}
	
	
	public static String[] getPictureFileList() {
        File rootsd = Environment.getExternalStorageDirectory();
        File dcim = new File(rootsd.getAbsolutePath() + "/DCIM/Camera");

        String[] pictureFileList = dcim.list(new FilenameFilter() {

            public boolean accept(File arg0, String arg1) {
                if (arg1.endsWith(".jpg"))
                    return true;
                return false;
            }
        });
        return pictureFileList;
    }
	
	public static String[] getPictureFileList(String filepath) {
		File dcim = new File(filepath);

		// String[] videoFileList = dcim.list();
		String[] pictureFileList = dcim.list(new FilenameFilter() {

			public boolean accept(File arg0, String arg1) {
				if (arg1.endsWith(".jpg") || arg1.endsWith(".png"))
					return true;
				return false;
			}
		});
		return pictureFileList;
	}

	

	public static void saveBitmap2SDCard(Context context, Bitmap b,
			File folder, int picnumber) {
		try {
			if(!folder.exists())
				folder.mkdir();
			OutputStream fOut = null;
			File file = new File(folder, picnumber + ".jpg");
			fOut = new FileOutputStream(file);

			b.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
			fOut.flush();
			fOut.close();

			MediaStore.Images.Media.insertImage(context.getContentResolver(),
					file.getAbsolutePath(), file.getName(), file.getName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	

	 
	  
}
