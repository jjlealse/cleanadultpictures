package br.com.gleicisantana.cleanadultpictures.learning;

import android.graphics.Bitmap;

import net.semanticmetadata.lire.imageanalysis.LireFeature;

public interface ImageFeatures {
    LireFeature extract(Bitmap bitmap);
    String fileNameModelArff();
}
