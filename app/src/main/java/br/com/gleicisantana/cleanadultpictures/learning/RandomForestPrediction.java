package br.com.gleicisantana.cleanadultpictures.learning;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import net.semanticmetadata.lire.imageanalysis.LireFeature;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;

import weka.classifiers.trees.RandomForest;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;

public class RandomForestPrediction implements Prediction {

    private RandomForest randomForest;
    private Context context;

    public RandomForestPrediction(Context context) {
        try {
            this.context = context;
            //InputStream i = context.getAssets().open("/mnt/sdcard/Adultclass/adult.model");
            randomForest = (RandomForest) SerializationHelper.read("/mnt/sdcard/Adultclass/adult.model");
            //train();
            //ObjectInputStream ois = new ObjectInputStream(i);
            //randomForest = (RandomForest) ois.readObject();
            //ois.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void train() throws Exception {
        InputStreamReader ir = new InputStreamReader(context.getAssets().open("adult_classifier_jpeg_filters.arff"));
        BufferedReader reader = new BufferedReader(ir);
        Instances instanceTrain = new Instances(reader);

        instanceTrain.setClassIndex(instanceTrain.numAttributes() - 1);
        reader.close();

        Log.e("WEKAAAA", "************* TREINANDO *****************");
        randomForest = new RandomForest();
        randomForest.buildClassifier(instanceTrain);

        SerializationHelper.write("/mnt/sdcard/Adultclass/adult.model", randomForest);
    }

    @Override
    public double[] distributionForInstance(Bitmap imgBmp) throws Exception {
        Instance instance = createInstance(imgBmp);
        return randomForest.distributionForInstance(instance);
    }

    @Override
    public String fileNameModel() {
        return "modelo_classificador_novo.model";
    }

    @Override
    public Instance createInstance(Bitmap bmp) {
        Log.e("WEKAAAA", "************* EXTRAINDO CARACTERISTICAS *****************");
        ImageFeatures imageFeatures = new JpegHistogramFeatures();
        LireFeature lireFeature = imageFeatures.extract(bmp);
        double[] features = lireFeature.getDoubleHistogram();
        int numFeatures = features.length;

        Instances instances = loadArffFile(imageFeatures);
        instances.setClassIndex(instances.numAttributes() - 1);

        Instance instance = new DenseInstance(numFeatures);

        instance.setDataset(instances);
        for (int index = 0; index < features.length; index++) {
            instance.setValue(index, features[index]);
        }
        instances.add(instance);

        return instance;
    }

    private Instances loadArffFile(ImageFeatures imageFeatures) {
        try {
            InputStreamReader ir = new InputStreamReader(context.getAssets().open(imageFeatures.fileNameModelArff()));
            BufferedReader reader = new BufferedReader(ir);
            Instances data = new Instances(reader);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
