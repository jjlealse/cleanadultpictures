package br.com.gleicisantana.cleanadultpictures.learning;

import android.graphics.Bitmap;

import weka.core.Instance;

public interface Prediction {

    double[] distributionForInstance(Bitmap imgBmp) throws Exception;
    String fileNameModel();
    Instance createInstance(Bitmap bmp);
}
