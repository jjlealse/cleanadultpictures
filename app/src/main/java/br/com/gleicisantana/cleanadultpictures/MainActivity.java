package br.com.gleicisantana.cleanadultpictures;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import br.com.gleicisantana.cleanadultpictures.learning.RandomForestPrediction;
import weka.classifiers.trees.RandomForest;
import weka.core.SerializationHelper;

public class MainActivity extends AppCompatActivity {

    private static final int SELECT_PICTURE_REQ_CODE = 1;

    private Button btnOpenGallery;
    private Button btnPrediction;
    private TextView txtResultPrediction;
    private ImageView imgSelected;

    private Bitmap imagemCarregada;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();

        btnOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setType("image/*");
                it.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(it, "Selecione uma imagem"),
                        SELECT_PICTURE_REQ_CODE);
            }
        });

        btnPrediction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagemCarregada != null) {
                    new AsyncTask<Void, Void, Void>() {
                        double[] prediction;
                        @Override
                        protected void onPreExecute() {
                            pDialog.show();
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                RandomForestPrediction randomForest = new RandomForestPrediction(MainActivity.this);
                                prediction = randomForest.distributionForInstance(imagemCarregada);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            pDialog.dismiss();

                            if (prediction != null) {
                                double percentPositive = prediction[0] * 100;
                                double percentNegative = prediction[1] * 100;

                                StringBuilder sb = new StringBuilder();
                                if (percentPositive > percentNegative && percentPositive >= 60) {
                                    sb.append("É pornografia com " + percentPositive + "% de confiança.");
                                } else {
                                    sb.append("Não é pornografia com " + percentNegative + "% de confiança.");
                                }
                                txtResultPrediction.setText(sb.toString());
                            }
                        }
                    }.execute();


                }
            }
        });
    }

    private void initComponents() {
        btnOpenGallery = findViewById(R.id.btnOpenGallery);
        btnPrediction = findViewById(R.id.btnPrediction);
        txtResultPrediction = findViewById(R.id.txtResultPrediction);
        imgSelected = findViewById(R.id.imgSelected);
        pDialog = new ProgressDialog(MainActivity.this);
    }

    private Bitmap resizeBitmap(Bitmap bmp) {
        Bitmap resized = Bitmap.createScaledBitmap(bmp, (int) (bmp.getWidth() * 0.3), (int) (bmp.getHeight() * 0.3), true);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG, 30, out);
        Bitmap compressed = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        return compressed;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == SELECT_PICTURE_REQ_CODE) {
                    Uri uriSelectedImage = data.getData();
                    imagemCarregada = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uriSelectedImage);
                    //imagemCarregada = resizeBitmap(imagemCarregada);
                    imgSelected.setImageBitmap(imagemCarregada);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
