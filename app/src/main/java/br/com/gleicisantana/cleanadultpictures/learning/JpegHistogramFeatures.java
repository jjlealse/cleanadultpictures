package br.com.gleicisantana.cleanadultpictures.learning;

import android.graphics.Bitmap;

import net.semanticmetadata.lire.imageanalysis.JpegCoefficientHistogram;
import net.semanticmetadata.lire.imageanalysis.LireFeature;

public class JpegHistogramFeatures implements ImageFeatures {
    @Override
    public LireFeature extract(Bitmap bitmap) {
        LireFeature jpegHistogram = new JpegCoefficientHistogram();
        jpegHistogram.extract(bitmap);
        return jpegHistogram;
    }

    @Override
    public String fileNameModelArff() {
        return "adult_classifier_model_att_jpeg_filter.arff";
    }
}
